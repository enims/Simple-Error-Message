<?php

// THIS GRABS THE FUNCTIONS FOR THE MESSAGE
include("functions.php");

// THIS IS AN EXAMPLE OF USING IT IN PROCESSING A FAILED LOGIN
// BE SURE TO INCLUDE THE CSS FILE IN A HEADER OR WHEREVER YOUR HTML IS

if(empty($_POST['username']) OR empty($_POST['password'])) {
    $the_user = FALSE;
    $the_pass = FALSE;
    error_message("You need to fill in a Username and Password!", "", "", "");
} else {
    $the_user = addslashes($_POST['username']);
    $the_pass = addslashes($_POST['password']);
}

?>