<?php

//Here are how the variables are setup:

$errormsg = 'The message you want to display';
$redir = 'Do you want to redirect Y=(1) or N=(0)';
$redir_to = 'what is the link you want to redirect to';
$pause_time = 'The amount of time for the person to read the error message in seconds 0=instant';

function error_message($errormsg, $redir, $redir_to, $pause_time) {
    echo "<div id=\"error_content_full\">
		".$errormsg."
	</div>";
    if($redir == '1') {
        echo "<meta http-equiv=\"refresh\" content=\"$pause_time;url=".$redir_to."\">";
    }
}

?>